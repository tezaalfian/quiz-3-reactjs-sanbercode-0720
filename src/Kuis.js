import React from "react";
import { MovieProvider } from "./template/MovieContext";
import Routes from "./template/Routes";

function Kuis() {
  return (
    <>
      <MovieProvider>
        <Routes />
      </MovieProvider>
    </>
  );
}

export default Kuis;
