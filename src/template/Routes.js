import React, { useContext } from "react";
import Footer from "./Footer";
import Header from "./Header";
import About from "../page/About";
import Home from "../page/Home";
import Movie from "../page/movie/Movie";
import Login from "../page/Login";
import { Switch, Route, Redirect } from "react-router-dom";
import { MovieContext } from "./MovieContext";

function Routes() {
  const [, , , , isLoggin] = useContext(MovieContext);
  return (
    <>
      <Header />
      <div class="wrapper">
        <Switch>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/movie-list">
            {isLoggin === false ? <Redirect to="/" /> : <Movie />}
          </Route>
          <Route exact path="/login">
            {isLoggin ? <Redirect to="/" /> : <Login />}
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </div>
      <Footer />
    </>
  );
}

export default Routes;
